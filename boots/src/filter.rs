use regex::Regex;

// The complexity of this file is due to the fact that there is no easy way to search all columns
// in SQL, so Filter must be expanded while the columns list are known. This means that there are
// two variants of Filter: Ready, and Unready.
//
// Ready means it is already expanded into a string. Unready means that it requires the columns to
// continue expansion.
// This is done by `ready_filter` in lib.rs.

#[derive(Debug, Clone)]
pub struct Filter {
    // string that goes after WHERE
    pub terms: Vec<Parsed>,
}

impl Filter {
    /// Make a filter that will not be parsed but directly placed after WHERE
    pub fn from_raw<T: Into<String>>(string: T) -> Self {
        Filter {
            terms: vec![Parsed::Ready(string.into())],
        }
    }
}

impl From<&str> for Filter {
    fn from(string: &str) -> Self {
        let nada = vec![Parsed::Ready("id LIKE \"%\"".to_string())];

        if string == "" {
            return Filter { terms: nada };
        }

        let re = Regex::new(
            r"(?x)
            (!)?   # possible negation
            (?:
            (\S+?) # key
            :      # colon
            )?
            (.*)   # value
        ",
        )
        .unwrap();

        let mut terms: Vec<Term> = Vec::new();

        for term in string.split(' ') {
            for cap in re.captures_iter(term) {
                let negation = cap.get(1).is_some();
                let key = cap.get(2).map(|m| m.as_str());
                let value = match cap.get(3) {
                    Some(v) => v.as_str().replace("_", " "),
                    None => return Filter { terms: nada },
                };
                terms.push(Term::new(negation, key, value));
            }
        }

        let terms: Vec<Parsed> = terms.iter().map(|t| t.parse()).collect();
        Filter { terms }
    }
}

#[derive(Debug, Clone)]
pub struct TableWideTerm {
    negation: bool,
    value: String,
}

impl TableWideTerm {
    fn new<T: Into<String>>(value: T, negation: bool) -> Self {
        TableWideTerm {
            negation,
            value: value.into(),
        }
    }

    pub fn to_string<I, T>(&self, columns: I) -> String
    where
        I: IntoIterator<Item = T>,
        T: Into<String> + std::fmt::Display,
    {
        let negation = if self.negation { "NOT" } else { "" };
        let mut filters = Vec::new();
        for column in columns {
            filters.push(format!("{} {} LIKE \"{}%\"", column, negation, self.value));
        }
        filters.join(" OR ")
    }
}

#[derive(Debug, Clone)]
pub enum Parsed {
    Ready(String),
    Unready(TableWideTerm),
}

#[derive(Debug)]
struct Term {
    negation: bool,
    key: Option<String>,
    value: String,
}

impl Term {
    fn new<T, U>(negation: bool, key: Option<T>, value: U) -> Self
    where
        T: Into<String>,
        U: Into<String>,
    {
        Term {
            negation,
            key: key.map(|m| m.into()),
            value: value.into(),
        }
    }

    fn parse(&self) -> Parsed {
        if self.negation {
            match self.key.clone() {
                Some(k) => Parsed::Ready(format!("{} NOT LIKE \"{}%\"", k, self.value)),
                None => Parsed::Unready(TableWideTerm::new(self.value.clone(), true)),
            }
        } else {
            match self.key.clone() {
                Some(k) => Parsed::Ready(format!("{} LIKE \"{}%\"", k, self.value)),
                None => Parsed::Unready(TableWideTerm::new(self.value.clone(), false)),
            }
        }
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn test_empty_str() {
//         assert_eq!(
//             Filter::from("").terms[0].to_string(),
//             "id like \"%\"".to_string()
//         )
//     }

//     #[test]
//     fn test_singles() {
//         assert_eq!(
//             Filter::from("id:1").terms[0].to_string(),
//             "id LIKE 1%".to_string()
//         )
//     }
// }

mod app;
mod errors;
mod input;

use errors::Error;

use boots::{Boot, Filter};
use std::collections::HashMap;
use std::ffi::OsStr;
use std::path::{Path, PathBuf};

use log::error;

type Result<T> = std::result::Result<T, Error>;

fn find_files<T: AsRef<Path>>(path: T) -> Result<Vec<PathBuf>> {
    let path = path.as_ref();
    let mut allpaths: Vec<PathBuf> = Vec::new();
    if path.is_dir() {
        for entry in path.read_dir()? {
            let path = entry?.path();
            if path.is_dir() {
                allpaths.extend(find_files(path)?);
            } else {
                allpaths.push(path);
            }
        }
    } else {
        allpaths.push(path.to_owned());
    }
    Ok(allpaths)
}

fn mv(config: &PathBuf) -> Result<()> {
    let boot = Boot::new(config)?;
    let moves = boot.update_paths()?;
    if moves.is_empty() {
        println!("No files moved.");
    } else {
        for (k, v) in moves {
            println!("{} -> {}", k, v);
        }
    }
    Ok(())
}

fn run() -> Result<()> {
    let matches = app::build_app().get_matches();

    // make log output user nice looking for cli-user
    loggerv::Logger::new()
        .verbosity(matches.occurrences_of("verbosity"))
        .level(true)
        .line_numbers(false)
        .separator(" ")
        .module_path(false)
        .colors(true)
        .init()
        .unwrap();

    // either use specified config path or default config path
    let config: PathBuf = match matches.value_of("config") {
        Some(config) => PathBuf::from(config),
        None => dirs::config_dir().unwrap().join("boots/config.toml"),
    };

    let boot = Boot::new(&config)?;

    match matches.subcommand() {
        // TODO should batch importing be in library or cli?
        ("import", Some(import_matches)) => {
            // safe unwrap since we require PATH
            let paths = import_matches.values_of("PATH").unwrap();
            let mut files = Vec::new();
            for path in paths {
                files.extend(find_files(path)?);
            }
            let files: Vec<PathBuf> = files
                .iter()
                .filter(|f| {
                    ["epub", "pdf", "cbz", "cbr"].contains(
                        &f.extension()
                            .unwrap_or_else(|| OsStr::new(""))
                            .to_string_lossy()
                            .to_string()
                            .as_ref(),
                    )
                })
                .map(|f| f.to_owned())
                .collect();

            let mut metadatas = Vec::new();
            let mut ids = Vec::new();
            for file in files {
                metadatas.push(boot.fetch_metadata(file.clone()));
                ids.push(file.to_string_lossy().to_string());
            }
            let inputs = input::edit(&boot.columns_req, &boot.columns_opt, &metadatas, ids)?;
            for input in inputs {
                let file = input.0;
                let metadata = input.1;
                boot.import(file, metadata)?;
            }
        }
        ("list", Some(list_matches)) => {
            let filter = Filter::from(list_matches.value_of("FILTER").unwrap_or(""));
            let list = boot.list(filter)?;
            println!("{}", list.join("\n"));
        }
        ("info", Some(info_matches)) => {
            let mut out = String::new();
            let filter = Filter::from(info_matches.value_of("FILTER").unwrap_or(""));
            let info = boot.info(filter)?;
            for result in info.iter() {
                for (k, v) in result {
                    let s = String::from("");
                    let v = v.as_ref().unwrap_or_else(|| &s);
                    out.push_str(k);
                    out.push_str(": ");
                    out.push_str(v);
                    out.push('\n');
                }
                out.push('\n');
            }
            println!("{}", out.trim());
        }
        ("move", Some(_)) => {
            mv(&config)?;
        }
        ("update", Some(_)) => {
            boot.update()?;
        }
        ("edit", Some(edit_matches)) => {
            let filter_string = edit_matches.value_of("FILTER").unwrap_or("");
            let filter = Filter::from(filter_string);
            let metadatas = boot.info(filter)?;
            // early return if no matches TODO make this universal
            if metadatas.is_empty() {
                println!("No matches found with filter: {}", filter_string);
                return Ok(());
            }
            // convert Vec of IndexMap to Vec of HashMap
            let metadatas: Vec<HashMap<String, String>> = metadatas
                .iter()
                .map(|m| {
                    m.iter()
                        .filter(|(_, v)| v.is_some())
                        .map(|(k, v)| (k.to_string(), v.as_ref().unwrap().to_string()))
                        .collect()
                })
                .collect();

            let ids: Vec<String> = metadatas
                .iter()
                .map(|x| x.get("id").unwrap().to_string())
                .collect();
            let inputs = input::edit(&boot.columns_req, &boot.columns_opt, &metadatas, ids)?;
            for input in inputs {
                let id = input.0;
                let metadata = input.1;

                let filter = Filter::from_raw(format!("id={}", id));

                boot.dbupdate(filter, &metadata)?;
            }
            mv(&config)?;
        }
        ("remove", Some(rm_matches)) => {
            let filter_string = rm_matches.value_of("FILTER").unwrap_or("");
            let filter = Filter::from(filter_string);
            let list = boot.list(filter.clone())?;

            if list.is_empty() {
                println!("No matches found with filter: {}", filter_string);
                return Ok(());
            }

            let deletefiles = rm_matches.is_present("deletefiles");
            if deletefiles {
                println!("Removing from db and deleting files:")
            } else {
                println!("Removing from db and NOT deleting files:")
            }

            println!("{}", list.join("\n"));

            let mut answer = String::new();
            println!("Yes/[No]");
            std::io::stdin().read_line(&mut answer)?;
            match answer.as_ref() {
                "Yes" => {
                    boot.delete(filter, deletefiles)?;
                }
                _ => println!("Removal Cancelled"),
            }
        }
        _ => unreachable!(),
    }
    Ok(())
}
fn main() {
    match run() {
        Ok(_) => {}
        Err(e) => error!("{}", e),
    }
}

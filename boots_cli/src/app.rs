use clap::{App, AppSettings, Arg, SubCommand};

pub fn build_app() -> App<'static, 'static> {
    App::new("boots")
        .version("0.1.0")
        .author("d_")
        .global_setting(AppSettings::UnifiedHelpMessage)
        .settings(&[
            AppSettings::ArgRequiredElseHelp,
            AppSettings::SubcommandRequiredElseHelp,
            AppSettings::ColoredHelp,
        ])
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .arg(
            Arg::with_name("config")
                .short("c")
                .value_name("FILE")
                .takes_value(true)
                .help("specify a config file"),
        )
        .subcommand(
            SubCommand::with_name("import").about("import a file").arg(
                Arg::with_name("PATH")
                    .required(true)
                    .multiple(true)
                    .help("book or document to import"),
            ),
        )
        .subcommand(
            SubCommand::with_name("list")
                .alias("ls")
                .about("list books")
                .arg(Arg::with_name("FILTER").help("tags or patterns to filter results")),
        )
        .subcommand(
            SubCommand::with_name("info")
                .about("list books and their info")
                .arg(Arg::with_name("FILTER").help("tags or patterns to filter results")),
        )
        .subcommand(SubCommand::with_name("update").about("update database to reflect filesystem"))
        .subcommand(
            SubCommand::with_name("move")
                .alias("mv")
                .about("update filesystem to reflect database"),
        )
        .subcommand(
            SubCommand::with_name("edit")
                .about("edit books' metadata")
                .arg(Arg::with_name("FILTER").help("tags or patterns to filter results")),
        )
        .subcommand(
            SubCommand::with_name("remove")
                .alias("rm")
                .about("remove books from the database")
                .arg(
                    Arg::with_name("deletefiles")
                        .short("d")
                        .long("delete")
                        .help("whether or not to also delete the files"),
                )
                .arg(Arg::with_name("FILTER").help("tags or patterns to filter results")),
        )
}
